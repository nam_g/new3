variable "team_bangerang" {
  description = "users in team bangerang"
  type = set(string)
  default     = [
    "abagail.hickle@example.com",
    "adalberto.ryan@example.com",
  ]
}
data "pagerduty_user" "team_bangerang" {
  for_each = var.team_bangerang
  email = each.value
}

variable "team_jet" {
  description = "users in team jet"
  type = set(string)
  default     = [
    "adeline.pfeffer@example.com"
  ]
}
data "pagerduty_user" "team_jet" {
  for_each = var.team_jet
  email = each.value
}


variable "atlas_sme" {
  description = "atlas smes"
  type = set(string)
  default     = [
    "ignacio.stoltenberg@example.com",
    "ivah.bruen@example.com",
    "jamie.daniel@example.com"
  ]
}
data "pagerduty_user" "atlas_sme" {
  for_each = var.atlas_sme
  email = each.value
}
