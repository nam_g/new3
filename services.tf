resource "pagerduty_service" "atlas" {
  name                    = "Atlas"
  auto_resolve_timeout    = 14400
  acknowledgement_timeout = 600
  escalation_policy       = pagerduty_escalation_policy.atlas-default-policy.id
  alert_creation          = "create_incidents"
}
