resource "pagerduty_schedule" "atlas-tier1-support" {
  name = "Daily Engineering Rotation"
  time_zone = "America/New_York"

  layer {
    name = "Day Shift"
    start = "2020-05-20T07:00:00-05:00"
    rotation_virtual_start = "2020-05-20T07:00:00-05:00"
    rotation_turn_length_seconds = 43200
    users = [
      for user in data.pagerduty_user.team_bangerang:
      user.id
    ]
  }

  layer {
    name = "Night Shift"
    start = "2020-05-20T07:00:00-05:00"
    rotation_virtual_start = "2020-05-20T19:00:00-05:00"
    rotation_turn_length_seconds = 43200
    users = [
      for user in data.pagerduty_user.team_jet:
      user.id
    ]
  }
}

resource "pagerduty_schedule" "atlas-tier2-support" {
  name = "Daily Engineering Rotation"
  time_zone = "America/New_York"

  layer {
    name = "24x7xsme"
    start = "2020-05-20T07:00:00-05:00"
    rotation_virtual_start = "2020-05-20T07:00:00-05:00"
    rotation_turn_length_seconds = 86400
    users = [
      for user in data.pagerduty_user.atlas_sme:
      user.id
    ]
  }
}