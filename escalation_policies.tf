resource "pagerduty_escalation_policy" "atlas-default-policy" {
  name      = "Engineering Escalation Policy"
  num_loops = 2

  rule {
    escalation_delay_in_minutes = 10

    target {
      type = "schedule"
      id   = pagerduty_schedule.atlas-tier1-support.id
    }
    target {
      type = "schedule"
      id   = pagerduty_schedule.atlas-tier2-support.id
    }
  }
}
